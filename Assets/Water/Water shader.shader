﻿Shader "Jonas/Water shader" {
	Properties {
		_Color ("Color", Color) = (0,0,1,1)
		_FoamColor ("Foam Color", Color) = (1,1,1,1)

		_InnerCut ("Color Cut", Range(0,1)) = 0.5

		_MainTex ("Albedo (RGB)", 2D) = "white" {}

		_Opacity ("Opacity", Range(0,1)) = 0.5

		//TRIPLANAR
		_TextureScale ("Texture Scale",float) = 1
		_TextureScale2 ("Texture Scale 2",float) = 1
		_TriplanarBlendSharpness ("Blend Sharpness",float) = 1

	}
	SubShader {
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows alpha

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _FloorTex;

		float _TextureScale;
		float _TextureScale2;
		float _TriplanarBlendSharpness;

		float _Opacity;

		struct Input {
			float2 uv_MainTex;
			float2 uv_FloorTex;

			float3 worldPos;
			float3 worldNormal;
		};

		half _InnerCut;
		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		fixed4 _FoamColor;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			
			// Find our UVs for each axis based on world position of the fragment.
			half2 yUV = IN.worldPos.xz / _TextureScale;
			half2 xUV = IN.worldPos.zy / _TextureScale;
			half2 zUV = IN.worldPos.xy / _TextureScale;
			// Now do texture samples from our diffuse map with each of the 3 UV set's we've just made.
			half3 yDiff = tex2D (_MainTex, yUV);
			half3 xDiff = tex2D (_MainTex, xUV);
			half3 zDiff = tex2D (_MainTex, zUV);

			// Get the absolute value of the world normal.
			// Put the blend weights to the power of BlendSharpness, the higher the value, 
            // the sharper the transition between the planar maps will be.
			half3 blendWeights = pow (abs(IN.worldNormal), _TriplanarBlendSharpness);

			// Divide our blend mask by the sum of it's components, this will make x+y+z=1
			blendWeights = blendWeights / (blendWeights.x + blendWeights.y + blendWeights.z);

			// Finally, blend together all three samples based on the blend mask.
			half3 color = xDiff * blendWeights.x + yDiff * blendWeights.y + zDiff * blendWeights.z ;

			fixed4 newColor = lerp(_Color, _FoamColor, step(_InnerCut,color.x) );

			o.Albedo = newColor;

			o.Alpha = _Opacity;

		}
		ENDCG
	}
	FallBack "Diffuse"
}
