﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipMovement : MonoBehaviour
{
    [SerializeField]
    float speed;

    private Touch touch;
    float camSensibility = 5;
    float touchSensibility = 0.15f;
    private float rotationY = 0f;
    private float rotationX = 0f;
    private float rotationZ = 0f;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {

            rotationY += (Input.GetAxis("Mouse X")) * camSensibility;
            rotationY = Mathf.Clamp(rotationY, -45, 45);

            rotationZ = Mathf.Clamp(rotationY, -15, 15);

            rotationX += (Input.GetAxis("Mouse Y")) * camSensibility;
            rotationX = Mathf.Clamp(rotationX, -45, 45);

            transform.localEulerAngles = new Vector3(rotationX, rotationY, rotationZ);
        }

        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Moved)
            {
                rotationY += touch.deltaPosition.x * touchSensibility;
                rotationY = Mathf.Clamp(rotationY, -45, 45);

                rotationX += touch.deltaPosition.y * touchSensibility;
                rotationX = Mathf.Clamp(rotationX, -45, 45);

                transform.localEulerAngles = new Vector3(rotationX, rotationY, transform.localEulerAngles.z);
            }
        }

        transform.Translate(0, 0, -speed * Time.deltaTime);

        /*Vector3 pos = Camera.main.WorldToViewportPoint(aim.position);
        pos.x = Mathf.Clamp01(pos.x);
        pos.y = Mathf.Clamp01(pos.y);
        aim.position = Camera.main.ViewportToWorldPoint(pos);*/
    }
}
