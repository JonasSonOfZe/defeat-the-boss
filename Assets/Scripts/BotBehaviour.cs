﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotBehaviour : MonoBehaviour
{

    [SerializeField]
    Transform player;

    [SerializeField]
    float speed;

    [SerializeField]
    ParticleSystem explosionParticle;

    [SerializeField]
    float frequency = 3.0f;
    [SerializeField]
    float magnitude = 0.2f;

    bool isDead;

    // Start is called before the first frame update
    void Start()
    {
        frequency = Random.Range(2.5f, 3.5f);
        magnitude = Random.Range(0.2f, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(player);

        transform.Translate(transform.forward * speed * Time.deltaTime + transform.right * Mathf.Sin(Time.time * frequency) * magnitude);

    }

    public void Die()
    {
        explosionParticle.Play();
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        isDead = true;
        Destroy(gameObject, 1);
    }

    public bool getDeath()
    {
        return isDead;
    }
}
