﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipAim : MonoBehaviour
{

    [SerializeField]
    RectTransform canvas;
    float canvasWidth;
    float canvasHeight;

    [SerializeField]
    Transform aimTarget;
    [SerializeField]
    RectTransform aim;

    Vector3 aimScrnPos;
    Vector3 aimScrnClampedPos;

    Vector3 shotEnd;
    [SerializeField]
    float fireRate = .25f;
    float nextFire = 0;
    WaitForSeconds shotDuration = new WaitForSeconds(.07f);

    LineRenderer lr;


    // Start is called before the first frame update
    void Start()
    {
        canvasWidth = canvas.rect.width;
        canvasHeight = canvas.rect.height;

        lr = gameObject.GetComponent<LineRenderer>();

        StartCoroutine("CheckAim");
    }

    // Update is called once per frame
    void Update()
    {
        //aim.localPosition = new Vector3(Mathf.Clamp(rotationY, -(canvasWidth / 2), canvasWidth / 2), Mathf.Clamp(rotationX, -(canvasHeight / 2), canvasHeight / 2), 0);
        aimScrnPos = Camera.main.WorldToScreenPoint(aimTarget.position);
        aimScrnClampedPos = new Vector3(Mathf.Clamp(aimScrnPos.x, 0, Screen.width), Mathf.Clamp(aimScrnPos.y, 0, Screen.height), 0);
        aim.position = aimScrnClampedPos;

    }


    IEnumerator CheckAim()
    {
        while (true)
        {
            //Vector3 shotDirection = (transform.position - camera.transform.position).normalized;

            RaycastHit hit;
            int LM = 1 << 9;
            Ray ray = Camera.main.ScreenPointToRay(aim.position);
            if (Physics.Raycast(ray, out hit))
            {
                
                if (hit.transform.gameObject.tag.Contains("Enemy") && Time.time > nextFire)
                {
                    BotBehaviour bot = hit.transform.gameObject.GetComponent<BotBehaviour>();
                    if (!bot.getDeath())
                    {
                        nextFire = Time.time + fireRate;

                        shotEnd = hit.point;
                        lr.SetPosition(0, transform.position);
                        lr.SetPosition(1, shotEnd);

                        StartCoroutine("ShotEffect");

                        bot.Die();
                    }

                    Debug.Log("hitou");
                }
            }
            else
            {
                Debug.Log("não hitou");
            }

            yield return new WaitForSeconds(.1f);
        }
    }

    IEnumerator ShotEffect()
    {
        lr.enabled = true;
        yield return shotDuration;
        lr.enabled = false;
    }
}